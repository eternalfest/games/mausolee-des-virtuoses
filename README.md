# Mausolée des virtuoses

Ancien message :
Crée par des vétérans pour garder le secret du Mausolée des Virtuoses… Afin de les obtenir, il faut déjà récupérer les 8 artefacts du Maître Ninja… 449 niveaux jouables !

Clone with HTTPS:
```shell
git clone https://gitlab.com/eternalfest/games/mausolee-des-virtuoses.git
```

Clone with SSH:
```shell
git clone git@gitlab.com:eternalfest/games/mausolee-des-virtuoses.git
```
