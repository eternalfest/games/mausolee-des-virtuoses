import bugfix.Bugfix;
import debug.Debug;
import hf.Hf;
import merlin.Merlin;
import game_params.GameParams;
import vault.Vault;
import patchman.IPatch;
import patchman.Patchman;
import atlas.Atlas;
import better_script.BetterScript;
import user_data.UserData;
import quests.QuestManager;
import etwin.Obfu;
import bat.Bat;
import disabled_controls.DisabledControls;
import display_texts.DisplayTexts;
import event_timer.EventTimer;
import mauso.Actions;
import mauso.Items;
import mauso.CustomMsg;
import mauso.quest.QuestInit;
import mauso.quest.QuestNinja;
import mauso.actions.EndGame;
import mauso.actions.Shuriken;

@:build(patchman.Build.di())
class Main {
  public static function main(): Void {
    Patchman.bootstrap(Main);
  }

  public function new(
    bugfix: Bugfix,
    debug: Debug,
    bat: Bat,
    items: Items,
    disabledControls: DisabledControls,
    displayTexts: DisplayTexts,
    customMsg: CustomMsg,
    atlasBoss: atlas.props.Boss,
    atlasDarkness: atlas.props.Darkness,
    atlas: atlas.Atlas,
    game_params: GameParams,
    event_timer: EventTimer,
    betterScript: BetterScript,
    vault: Vault,
    endGame: EndGame,
    shuriken: Shuriken,
    questInit: QuestInit,
    quests: QuestManager,
    actions: Actions,
    merlin: Merlin,
    user_data: UserData,
    patches: Array<IPatch>,
    hf: Hf
  ) {
    quests.register_reward(Obfu.raw("ninja"), new QuestNinja());
    Patchman.patchAll(patches, hf);
  }
}
