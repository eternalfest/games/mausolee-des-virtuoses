package bat;

import patchman.DebugConsole;
import etwin.Error;
import patchman.module.Data;
import etwin.Obfu;

@:build(patchman.Build.di())
class BatConfig {
    public var lives(default, null): Null<Int>;
    public var speed(default, null): Null<Float>;
    public var wait_time(default, null): Null<Float>;
    public var launches_fireball(default, null): Bool;
    public var modifies_rotating_fireball(default, null): Bool;
    public var rotating_fireball_list(default, null): Null<Array<Array<Float>>>;
    public var rotating_fireball_global_distance(default, null): Null<Float>;
    public var rotating_fireball_speed(default, null): Null<Float>;
    public var delay_between_launched_fireballs(default, null): Float;
    public var delay_before_first_launched_fireball(default, null): Float;
    public var angle_between_launched_fireballs(default, null): Float;
    public var launched_fireball_speed(default, null): Float;
    public var nb_launched_fireballs(default, null): Int;
    public var nb_launched_fireball_at_once(default, null): Int;
    public var launched_fireball_internal_angle(default, null): Float;

    public function new(lives: Null<Int>, speed: Null<Float>, wait_time: Null<Float>, launches_fireball: Bool, modifies_rotating_fireball: Bool,
                        rotating_fireball_list: Null<Array<Array<Float>>>, rotating_fireball_global_distance: Null<Float>, rotating_fireball_speed: Null<Float>,
                        delay_between_launched_fireballs: Float, delay_before_first_launched_fireball: Float, angle_between_launched_fireballs: Float, launched_fireball_speed: Float,
                        nb_launched_fireballs: Int, nb_launched_fireball_at_once: Int, launched_fireball_internal_angle: Float): Void {
        this.lives = lives;
        this.speed = speed;
        this.wait_time = wait_time;
        this.launches_fireball = launches_fireball;
        this.modifies_rotating_fireball = modifies_rotating_fireball;
        this.rotating_fireball_list = rotating_fireball_list;
        this.delay_between_launched_fireballs = delay_between_launched_fireballs;
        this.delay_before_first_launched_fireball = delay_before_first_launched_fireball;
        this.angle_between_launched_fireballs = angle_between_launched_fireballs;
        this.launched_fireball_speed = launched_fireball_speed;
        this.nb_launched_fireballs = nb_launched_fireballs;
        this.nb_launched_fireball_at_once = nb_launched_fireball_at_once;
        this.launched_fireball_internal_angle = launched_fireball_internal_angle;
    }

    public static var DEFAULT: BatConfig = new BatConfig(null, null, null, false, false, null, null, null, 0, 0, 0, 0, 0, 0, 0);

    private static var BAT_CONFIG(default, never): String = Obfu.raw("CS");
    private static var BOSS(default, never): String = Obfu.raw("boss");
    private static var LIVES(default, never): String = Obfu.raw("life");
    private static var SPEED(default, never): String = Obfu.raw("speed");
    private static var WAIT_TIME(default, never): String = Obfu.raw("waitTime");
    private static var LAUNCH_FIREBALLS(default, never): String = Obfu.raw("launchFireballs");
    private static var ROTATING_FIREBALLS(default, never): String = Obfu.raw("rotatingFireballs");
    private static var LIST(default, never): String = Obfu.raw("list");
    private static var GLOBAL_DISTANCE(default, never): String = Obfu.raw("globalDistance");
    private static var DELAY(default, never): String = Obfu.raw("delay");
    private static var DELAY_BEFORE_FIRST(default, never): String = Obfu.raw("delayBeforeFirst");
    private static var ANGLE(default, never): String = Obfu.raw("angle");
    private static var NUMBER(default, never): String = Obfu.raw("number");
    private static var INTERN_NUMBER(default, never): String = Obfu.raw("internNumber");
    private static var INTERN_ANGLE(default, never): String = Obfu.raw("internAngle");

    @:diFactory
    public static function fromHml(dataMod: Data): BatConfig {
        if (!dataMod.has(BAT_CONFIG)) {
            DebugConsole.error("Les données BossChauveSouris ne sont pas présentes.");
            return BatConfig.DEFAULT;
        }

        var data: Dynamic = dataMod.get(BAT_CONFIG);

        var boss: Dynamic = Reflect.field(data, BOSS);
        if (boss == null) {
            DebugConsole.error("Les données BossChauveSouris.boss ne sont pas présentes.");
            return BatConfig.DEFAULT;
        }

        var launches_fireballs = Reflect.field(boss, LAUNCH_FIREBALLS);
        var launch_fireball = Reflect.field(data, LAUNCH_FIREBALLS);

        if (launches_fireballs && launch_fireball == null) {
            DebugConsole.error("Les données BossChauveSouris.launchFireballs ne sont pas présentes.");
            return BatConfig.DEFAULT;
        }

        var rotating_fireballs = Reflect.field(data, ROTATING_FIREBALLS);
        var has_rotating_fireballs = rotating_fireballs == null;

        var lives = Reflect.field(boss, LIVES);
        var speed = Reflect.field(boss, SPEED);
        var wait_time = Reflect.field(boss, WAIT_TIME);
        var rotating_fireball_list = Reflect.field(rotating_fireballs, LIST);
        var rotating_fireball_global_distance = Reflect.field(rotating_fireballs, GLOBAL_DISTANCE);
        var rotating_fireball_speed = Reflect.field(rotating_fireballs, SPEED);
        var delay = Reflect.field(launch_fireball, DELAY);
        var delay_before_first = Reflect.field(launch_fireball, DELAY_BEFORE_FIRST);
        var angle = Reflect.field(launch_fireball, ANGLE);
        var filreball_speed = Reflect.field(launch_fireball, SPEED);
        var number = Reflect.field(launch_fireball, NUMBER);
        var intern_number = Reflect.field(launch_fireball, INTERN_NUMBER);
        var intern_angle = Reflect.field(launch_fireball, INTERN_ANGLE);
        if (launches_fireballs && (delay == null || delay_before_first == null || angle == null || speed == null || number == null || intern_number == null || intern_angle == null)) {
            DebugConsole.error("Une partie des données de BossChauveSouris.launchFireballs ne sont pas présentes.");
            return BatConfig.DEFAULT;            
        }

        return new BatConfig(lives, speed, wait_time, launches_fireballs, has_rotating_fireballs, rotating_fireball_list, rotating_fireball_global_distance, rotating_fireball_speed, delay, delay_before_first, angle, filreball_speed, number, intern_number, intern_angle);
    }
}