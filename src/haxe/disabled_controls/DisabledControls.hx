package disabled_controls;

import hf.Hf;
import hf.Mode;
import hf.mode.GameMode;
import hf.entity.PlayerController;
import etwin.ds.FrozenArray;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;

@:build(patchman.Build.di())
class DisabledControls {

  @:diExport
  public var patches(default, null): IPatch;

  @:diExport
  public var actions(default, null): IAction;
    
  public var setControls: Bool;

  public function new() {
    this.actions = new DisableControls(this);
      
    this.setControls = false;

    this.patches = new PatchList([

      Ref.auto(PlayerController.getControls).wrap(function(hf, self: PlayerController, old): Void {
        if (!this.setControls) {
		  old(self);
		}
      }),
    ]);
  }
}

class DisableControls implements IAction {
  public var name(default, null): String = Obfu.raw("disableControls");
  public var isVerbose(default, null): Bool = false;

  private var mod: DisabledControls;
    
  public function new(mod: DisabledControls) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();

    var controls: Bool = ctx.getOptBool(Obfu.raw("n")).or(true);
    this.mod.setControls = controls;
    hf.Data.WAIT_TIMER = (controls ? hf.Data.SECOND * 99999 : hf.Data.SECOND * 8);

    return false;
  }
}
