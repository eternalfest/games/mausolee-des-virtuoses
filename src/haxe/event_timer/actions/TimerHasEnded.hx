package event_timer.actions;

import hf.Hf;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;
import etwin.ds.WeakMap;

class TimerHasEnded implements IAction {
  public var name(default, null): String = Obfu.raw("timerHasEnded");
  public var isVerbose(default, null): Bool = false;
    
  private var mod: EventTimer;

  public function new(mod: EventTimer) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();
    
    var result: Bool = false;
    if(EventTimer.TIMER_HAS_ENDED.get(game)) {
      result = true; 
    }

    return result;
  }
}
