package event_timer.actions;

import hf.Hf;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;
import etwin.ds.WeakMap;

class TimerReinit implements IAction {
  public var name(default, null): String = Obfu.raw("timerReinit");
  public var isVerbose(default, null): Bool = false;
    
  private var mod: EventTimer;

  public function new(mod: EventTimer) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var minutes: Int = ctx.getOptInt(Obfu.raw("min")).or(0);
    var secondes: Int = ctx.getOptInt(Obfu.raw("s")).or(0);
    var level: Bool = ctx.getOptBool(Obfu.raw("lvl")).or(false);
    var effects: Bool = ctx.getOptBool(Obfu.raw("effects")).or(true);
    var game: GameMode = ctx.getGame();
      
    if(minutes < 0) minutes = 0;
    if(secondes > 59 || secondes < 0) secondes = 0;

    EventTimer.TIMER_LEVEL.set(game, level);
      
    if(!effects && (secondes >= 10 || minutes > 0)) {
      EventTimer.TIMER_EFFECTS.set(game, true);
    } else {
      EventTimer.TIMER_EFFECTS.set(game, false); 
    }
      
    EventTimer.TIMER_MINUTES.set(game, minutes);
    EventTimer.TIMER_SECONDES.set(game, secondes);
    EventTimer.TIMER_CENTIEMES.set(game, 0);
      
    return false;
  }
}
