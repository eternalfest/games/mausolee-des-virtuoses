package event_timer.actions;

import hf.Hf;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;
import etwin.ds.WeakMap;

class TimerStart implements IAction {
  public var name(default, null): String = Obfu.raw("timerStart");
  public var isVerbose(default, null): Bool = false;
    
  private var mod: EventTimer;

  public function new(mod: EventTimer) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var minutes: Int = ctx.getOptInt(Obfu.raw("min")).or(1);
    var secondes: Int = ctx.getOptInt(Obfu.raw("s")).or(0);
    var centiemes = 0;
    var level: Bool = ctx.getOptBool(Obfu.raw("lvl")).or(false);
    var effects: Bool = ctx.getOptBool(Obfu.raw("effects")).or(true);
    var txt: String = ctx.getString(Obfu.raw("txt"));
    var hf: Hf = ctx.getHf();
    var game: GameMode = ctx.getGame();
      
    if(secondes > 59 || secondes < 0) {
      secondes = 0;
    }
    if(minutes < 0) minutes = 1;
    
    EventTimer.TIMER_ACTIVATE.set(game, true);
    EventTimer.TIMER_MINUTES.set(game, minutes);
    EventTimer.TIMER_SECONDES.set(game, secondes);
    EventTimer.TIMER_CENTIEMES.set(game, centiemes);
    EventTimer.TIMER_LEVEL.set(game, level);
    EventTimer.TIMER_TEXT_SCALE.set(game, 150);
    EventTimer.TIMER_MAX_TEXT_SCALE.set(game, 150);
    EventTimer.TIMER_HAS_ENDED.set(game, false);
      
    if(!effects && (secondes >= 10 || minutes > 0)) {
      EventTimer.TIMER_EFFECTS.set(game, true);
    } else {
      EventTimer.TIMER_EFFECTS.set(game, false); 
    }
      
    this.mod.timerMessageCentre(hf, game.fxMan, txt);
      
    return false;
  }
}
