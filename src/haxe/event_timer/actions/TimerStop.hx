package event_timer.actions;

import hf.Hf;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.Obfu;
import etwin.ds.WeakMap;

class TimerStop implements IAction {
  public var name(default, null): String = Obfu.raw("timerStop");
  public var isVerbose(default, null): Bool = false;
    
  private var mod: EventTimer;

  public function new(mod: EventTimer) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();

    EventTimer.TIMER_ACTIVATE.set(game, false);
    this.mod.timerRemove(hf, game.fxMan);
      
    return false;
  }
}
