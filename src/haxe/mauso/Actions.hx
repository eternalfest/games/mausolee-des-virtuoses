package mauso;

import etwin.ds.FrozenArray;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import merlin.IAction;
import merlin.IRefFactory;
import mauso.actions.EventWall;
import mauso.actions.EndText;
import mauso.actions.Lightning;

@:build(patchman.Build.di())
class Actions {
  @:diExport
  public var actions(default, null): FrozenArray<IAction>;

  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;
  @:diExport
  public var eventWall(default, null): IAction;
  @:diExport
  public var endText(default, null): IAction;
  @:diExport
  public var lightning(default, null): IAction;
    
  public function new() {
    this.patches = FrozenArray.from(patches);
    this.eventWall = new EventWall();
    this.endText = new EndText();
    this.lightning = new Lightning();
  }
}
