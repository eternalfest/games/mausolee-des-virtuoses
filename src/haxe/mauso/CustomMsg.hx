package mauso;

import mauso.actions.CustomMsgAction;

import patchman.IPatch;
import etwin.ds.FrozenArray;
import etwin.Obfu;
import merlin.IAction;
import patchman.module.Run;
import ef.api.run.IRun;

@:build(patchman.Build.di())
class CustomMsg {

    @:diExport
    public var actions(default, null): FrozenArray<IAction>;
    @:diExport
    public var patches(default, null): FrozenArray<IPatch>;

    private var run: Run;

    public var customTexts: Map<String, String> = [
        Obfu.raw("msg_6102") => "*Igor bleu* : Ton nom est *%username%* ? Le mien également. ",
        Obfu.raw("msg_6103") => "*L'autre %username%* : Je venais de vaincre Tuberculoz, j'ai vu des étoiles de couleurs et tout à coup, aucun souvenir.",
        Obfu.raw("msg_6104") => "*L'autre %username%* : Les Éternels, tu dis ? Non, je ne connais pas. Tu penses qu'ils auraient une idée de ce qui est arrivé ?",
        Obfu.raw("msg_6105") => "*L'autre %username%* : J'espère qu'ils comprendront pourquoi nous avons le même nom.",
        Obfu.raw("msg_6106") => "*L'autre %username%* : Désolé de t'avoir attaqué en tout cas... On peut dire en quelque sorte que<br/>*tu t'es battu toi-même*.",
        Obfu.raw("msg_6107") => "*L'autre %username%* : Prends cette sorte d'orbe avec toi, cela a peut-être un rapport avec mon état."
    ];


    public function new(run: Run) {
        this.run = run;

        this.actions = FrozenArray.of(
            (new CustomMsgAction(this): IAction)
        );

        var patches = [];

        this.patches = FrozenArray.from(patches);
    }

    public function customMsg(game: hf.mode.GameMode, id: String): Void {
        var getRun = this.run.getRun();
        game.attachPop('\n' + game.root.Tools.replace(customTexts[id], '%username%', getRun.user.displayName), false);
    }
}
