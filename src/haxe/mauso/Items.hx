package mauso;

import etwin.ds.FrozenArray;
import patchman.IPatch;
import vault.IItem;
import vault.ISpec;
import vault.ISkin;

import mauso.items.CustomPearl;
import mauso.items.CustomShuriken;
import mauso.items.CustomOcarina;
import mauso.items.CustomCasque;
import mauso.items.CustomPaix;
import mauso.items.CustomNajinata;
import mauso.items.CustomBaton;
import mauso.items.CustomInsigne;
import mauso.items.CustomDelice;
import mauso.items.CustomFraise;
import mauso.items.CustomFramboise;
import mauso.items.CustomPassHaut;
import mauso.items.CustomPassGauche;
import mauso.items.CustomPassDroite;
import mauso.items.CustomPassBas;

@:build(patchman.Build.di())
class Items {
  @:diExport
  public var items(default, null): FrozenArray<IItem>;
  @:diExport
  public var skins(default, null): FrozenArray<ISkin>;

  @:diExport
  public var patches(default, null): FrozenArray<IPatch>;

  public function new(customPearl: CustomPearl,
                      patches: Array<IPatch>) {
    this.patches = FrozenArray.from(patches);
    this.items = FrozenArray.of(
      (customPearl: IItem)
    );
      
    this.skins = FrozenArray.of(
      new CustomShuriken(),
      new CustomOcarina(),
      new CustomCasque(),
      new CustomPaix(),
      new CustomNajinata(),
      new CustomBaton(),
      new CustomInsigne(),
      new CustomDelice(),
      new CustomFraise(),
      new CustomFramboise(),
      new CustomPassHaut(),
      new CustomPassGauche(),
      new CustomPassDroite(),
      new CustomPassBas()
    );
  }
}
