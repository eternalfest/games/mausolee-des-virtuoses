package mauso.actions;

import etwin.Obfu;
import merlin.IAction;
import merlin.IActionContext;

class CustomMsgAction implements IAction {
    public var name(default, null): String = Obfu.raw("customMsg");
    public var isVerbose(default, null): Bool = false;

    private var mod: CustomMsg;

    public function new(mod: CustomMsg) {
        this.mod = mod;
    }

    public function run(ctx: IActionContext): Bool {
        var game: hf.mode.GameMode = ctx.getGame();
        var id: String = ctx.getString(Obfu.raw("id"));

        this.mod.customMsg(game, id);

        return false;
    }
}