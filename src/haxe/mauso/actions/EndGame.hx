package mauso.actions;

import hf.Hf;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import etwin.Obfu;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;

@:build(patchman.Build.di())
class EndGame {

  @:diExport
  public var patches(default, null): IPatch;

  @:diExport
  public var actions(default, null): IAction;
    
  public var end: Bool;
  public var timer: Int;

  public function new() {
    this.actions = new EndingGame(this);
      
    this.end = false;
    this.timer = 120;

    this.patches = new PatchList([

      Ref.auto(hf.mode.GameMode.main).after(function(hf: hf.Hf, self: hf.mode.GameMode): Void {
        if(end) {
          self.dfactor++;
          self.targetDark++;
          self.darknessMC._alpha++;
          var shift: Bool = true;
          var players = self.getPlayerList();
          var i: Int = 0;
          var halo: MovieClip;
          
          for(i in 0...players.length) {
            halo = self.darknessMC.holes[i];
            if(halo == null) continue;
            halo._xscale -= 2;
            if(halo._xscale <= 0) halo._xscale = 0;
            halo._yscale -= 2;
            if(halo._yscale <= 0) halo._yscale = 0;
            if(halo._xscale != 0 || self.darknessMC._alpha < 200) shift = false;
          }
          for(i in 0...self.extraHoles.length){
	        self.extraHoles[i].mc.removeMovieClip();
	      }
          self.detachExtraHoles();
            
          if(shift) {
            timer--;
            if(timer <= 0) self.goto(666);
          }
        }
          
      }),
    ]);
  }
}

class EndingGame implements IAction {
  public var name(default, null): String = Obfu.raw("endGame");
  public var isVerbose(default, null): Bool = false;

  private var mod: EndGame;
    
  public function new(mod: EndGame) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    this.mod.end = true;
    return false;
  }
    
}