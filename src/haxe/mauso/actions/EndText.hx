package mauso.actions;

import hf.Hf;
import hf.FxManager;
import hf.mode.GameMode;
import hf.levels.ScriptEngine;
import merlin.IAction;
import merlin.IActionContext;
import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import etwin.Obfu;

class EndText implements IAction {
  public var name(default, null): String = Obfu.raw("endText");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var txt: String = ctx.getString(Obfu.raw("txt"));
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();
    
    this.createNormalTextSprite(hf, game, txt, 290, 460, 150, hf.Data.DP_SOUNDS);
      
    return false;
  }
    
  private function createNormalTextSprite(hf: Hf, game: hf.mode.GameMode, text: String, x: Int, y: Int, scale: Int, layer): MovieClip {
    if(layer == null) layer = hf.Data.DP_TOP;
    var text_sprite = game.depthMan.attach('hammer_interf_inGameMsg', layer);
    (cast text_sprite).field._width = 800;
    (cast text_sprite).label.text = "";
    (cast text_sprite).field.text = text;
    (cast text_sprite).field._xscale = scale;
    (cast text_sprite).field._yscale = scale;
    (cast text_sprite).field._x = x;
    (cast text_sprite).field._y = y;
    hf.FxManager.addGlow(text_sprite, 0, 2);
    return text_sprite;
  }
}