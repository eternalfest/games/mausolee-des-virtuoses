package mauso.actions;

import hf.Hf;
import hf.Entity;
import hf.mode.GameMode;
import hf.levels.ScriptEngine;
import merlin.IAction;
import merlin.IActionContext;
import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import etwin.Obfu;

class EventWall implements IAction {
  public var name(default, null): String = Obfu.raw("eventWall");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var x: Float = ctx.getOptInt(Obfu.raw("x")).or(0);
    var y: Float = ctx.getOptInt(Obfu.raw("y")).or(0);
    var len: Int = ctx.getOptInt(Obfu.raw("len")).or(0);
    var sid: Int = ctx.getOptInt(Obfu.raw("sid")).or(0);
    var dir: String = ctx.getString(Obfu.raw("dir"));
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();
    var wallHoriz: Bool;
    if(dir == Obfu.raw("up") || dir == Obfu.raw("down")) {
      wallHoriz = true;
    } else {
      wallHoriz = false;    
    }
    
    var dxSpike: Int = dir == Obfu.raw("left") ? -1 : (dir == Obfu.raw("right") ? 1 : 0);
    var dySpike: Int = dir == Obfu.raw("up") ? -1 : (dir == Obfu.raw("down") ? 1 : 0);
      
    var killBads: Bool = true;

    if(game.fl_mirror) {
      x = game.flipCoordCase(x) - len;
      dxSpike *= -1;
    }
    
    var bads = game.getBadClearList();
    
    for(i in 0...len) {
      game.world.scriptEngine.killById(sid + i);
      game.world.forceCase(cast x, cast y, hf.Data.GROUND);
      game.world.scriptEngine.fl_redraw = true;
        
      var xSpike: Float = x + dxSpike;
      var ySpike: Float = y + dySpike;
        
      if (game.world.getCase({ x: cast xSpike, y: cast ySpike }) <= 0) {
        xSpike = hf.Entity.x_ctr(xSpike) - hf.Data.CASE_WIDTH * 0.5;
        ySpike = hf.Entity.y_ctr(ySpike - 1);
        var s = game.attachBad(13, xSpike, ySpike);
        var spike: DynamicMovieClip = cast s;
        spike.scriptId = sid + i;
        game.world.scriptEngine.mcList[sid + i].mc = spike;
          
        if(killBads) {
          for(j in 0...bads.length) {
            var bad = cast bads[j];
            if(!bad.fl_kill && bad.hitBound(spike)) {
              spike.hit(bad);
            }
          }
        }
      }

      if(wallHoriz) {
        x++;
      } else {
        y++;
      }
    }
    
    return false;
  }
}