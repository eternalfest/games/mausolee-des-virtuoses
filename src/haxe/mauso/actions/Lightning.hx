package mauso.actions;

import hf.Hf;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import etwin.Obfu;

class Lightning implements IAction {
  public var name(default, null): String = Obfu.raw("lightning");
  public var isVerbose(default, null): Bool = false;

  public function new() {}

  public function run(ctx: IActionContext): Bool {
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();
      
    var target = game.getOne(hf.Data.PLAYER);
    this.createBadLightning(hf, game, target.x, 60);
      
    return false;
  }
    
  private function createBadLightning(hf: Hf, game: hf.mode.GameMode, x: Float, time_before_strike: Int): Void {
    var lightning = game.depthMan.attach("hammer_fx_death", hf.Data.FX);
    (cast lightning)._x = x;
	(cast lightning)._y = hf.Data.GAME_HEIGHT * 0.5;
    (cast lightning)._xscale = 80;
    (cast lightning).initial_effects_done = false;
    game.addToList(hf.Data.ENTITY, (cast lightning));
    game.addToList(hf.Data.SHOOT, (cast lightning));
    (cast lightning).time = time_before_strike;
    (cast lightning).game = game;
      
    (cast lightning).update = function(){
      (cast lightning).time -= hf.Timer.tmod;
      if((cast lightning).time > 0) {
        (cast lightning).gotoAndStop('1');
        return;
      }

      if (!(cast lightning).initial_effects_done) {
        (cast lightning).initial_effects_done = true;
        game.shake(10, 3);
        game.soundMan.playSound('sound_bad_death', hf.Data.CHAN_BAD);
      }
        
      var players = game.getPlayerList();
      var i: Int = 0;
      for(i in 0...players.length) {
        var player = players[i];
        if (player.x > (cast lightning)._x - 15 && player.x < (cast lightning)._x + 15) player.killHit(null);
      }
      (cast lightning).play('1');
    };
      
    (cast lightning).destroy = function() {(cast lightning).removeMovieClip();};
    (cast lightning).onUnload = function() {game.removeFromList(hf.Data.ENTITY, (cast lightning));};
  }
}