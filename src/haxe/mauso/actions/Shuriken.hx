package mauso.actions;

import hf.Hf;
import hf.Entity;
import hf.entity.item.ScoreItem;
import hf.mode.GameMode;
import merlin.IAction;
import merlin.IActionContext;
import etwin.flash.MovieClip;
import etwin.flash.DynamicMovieClip;
import etwin.Obfu;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;

@:build(patchman.Build.di())
class Shuriken {

  @:diExport
  public var actions(default, null): IAction;    

  public function new() {
    this.actions = new ThrowShuriken(this);
  }
}

class ThrowShuriken implements IAction {
  public var name(default, null): String = Obfu.raw("shuriken");
  public var isVerbose(default, null): Bool = false;

  private var mod: Shuriken;
    
  public function new(mod: Shuriken) {
    this.mod = mod;
  }

  public function run(ctx: IActionContext): Bool {
    var x: Float = ctx.getOptInt(Obfu.raw("x")).or(Std.random(400));
    var y: Float = ctx.getOptInt(Obfu.raw("y")).or(Std.random(500));
    var fast: Float = ctx.getOptInt(Obfu.raw("fast")).or(50);
    var game: GameMode = ctx.getGame();
    var hf: Hf = ctx.getHf();
      
    this.createShuriken(hf, game, x, y, Std.random(100) < fast);
    return false;
  }
    
  public function createShuriken(hf: Hf, game: GameMode, posX: Float, posY: Float, fast: Bool){
    var sprite: ScoreItem = cast game.world.view.attachSprite('hammer_item_score', posX, posY, false);
    sprite.fl_gravity = false;
    sprite.fl_hitWall = false;
    sprite.fl_hitBorder = false;
    sprite.fl_hitGround = false;
    sprite.fl_friction = false;
    sprite.fl_bump = false;
    (cast sprite).width = sprite._width;
    (cast sprite).height = sprite._height;
    (cast sprite).angSpeed = 12 + Std.random(5);
		
    if(!fast) (cast sprite).angSpeed *= 0.8;
			
    (cast sprite).update = function(){
      Reflect.callMethod(sprite, untyped hf.entity.Physics.prototype.update, []);
			
      if(!sprite.game.world.inBound(sprite.cx, Math.floor(Math.max(sprite.cy, 0)))) {
        sprite.destroy();
      }
      
    };
  
    (cast sprite).endUpdate = function(){
      Reflect.callMethod(sprite, untyped hf.Entity.prototype.endUpdate, []);
			
      var rot_rad: Float = sprite.rotation * (Math.PI / 180);

      sprite._x -= (cast sprite).width * Math.sin(rot_rad) / 2;
      sprite._y += (cast sprite).height * Math.cos(rot_rad) / 2;

      sprite.rotation += (cast sprite).angSpeed; 
    };
      
    (cast sprite).hit = function(entity: Entity){
      if((entity.types & hf.Data.PLAYER) > 0){
        (cast entity).killHit(sprite.dx);
      }
    };
	
    Reflect.callMethod(sprite, untyped hf.entity.Physics.prototype.init, [game]);
    sprite.register(hf.Data.SHOOT);
    sprite.moveTo(posX, posY);
	
    sprite.gotoAndStop("" + ((fast?236:228) + 1));
    sprite.sub.gotoAndStop("0");
    sprite.endUpdate();
		
    sprite.moveToTarget(game.getOne(hf.Data.PLAYER), fast?10:7);
  }
    
}