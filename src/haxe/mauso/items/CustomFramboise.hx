package mauso.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import vault.ISkin;

class CustomFramboise implements ISkin {
  public static var ID_ITEM: Int = 165;
  public var id(default, null): Int = 1249;
  public var sprite(default, null): Null<String> = null;

  public function new() {}

  public function skin(mc: MovieClip, subId: Null<Int>): Void {
    mc.gotoAndStop("" + (ID_ITEM + 1));
  }
}