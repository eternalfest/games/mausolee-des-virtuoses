package mauso.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import vault.ISkin;

class CustomNajinata implements ISkin {
  public static var ID_ITEM: Int = 232;
  public var id(default, null): Int = 1244;
  public var sprite(default, null): Null<String> = null;

  public function new() {}

  public function skin(mc: MovieClip, subId: Null<Int>): Void {
    mc.gotoAndStop("" + (ID_ITEM + 1));
  }
}