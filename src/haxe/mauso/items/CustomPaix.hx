package mauso.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import vault.ISkin;

class CustomPaix implements ISkin {
  public static var ID_ITEM: Int = 6;
  public var id(default, null): Int = 1243;
  public var sprite(default, null): Null<String> = "hammer_item_special";

  public function new() {}

  public function skin(mc: MovieClip, subId: Null<Int>): Void {
    mc.gotoAndStop("" + (ID_ITEM + 1));
  }
}