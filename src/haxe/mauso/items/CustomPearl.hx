package mauso.items;

import etwin.flash.MovieClip;
import hf.entity.Item;
import hf.Hf;
import hf.SpecialManager;
import hf.mode.GameMode;
import vault.IItem;
import patchman.IPatch;
import patchman.PatchList;
import patchman.Ref;
import etwin.flash.filters.ColorMatrixFilter;
import color_matrix.ColorMatrix;

@:build(patchman.Build.di())
class CustomPearl implements IItem {
  public static var ID_PEARL: Int = 98;
  public var id(default, null): Int = 118;
  public var isActive: Bool = false;
  public var sprite(default, null): Null<String> = null;

  @:diExport
  private var patch(default, null): IPatch;

  public function new(): Void {
    this.patch = new PatchList([
 
      Ref.auto(SpecialManager.levelConversion).wrap(function(hf: Hf, self: SpecialManager, id: Int, sid: Int, old): Void {

        if (isActive) {
          self.game.world.scriptEngine.safeMode();
          self.game.killPop();
          var nbPierres: Int = 0;
          var x: Int = 0;
          var y: Int = 0;
          for(y in 0...hf.Data.LEVEL_HEIGHT) {
            for(x in 0...hf.Data.LEVEL_WIDTH) {
              if(self.game.world.checkFlag({x: x, y: y}, hf.Data.IA_TILE_TOP)) {
                var time = nbPierres * 2;
                if(nbPierres < 4) time = 1;
                self.game.world.scriptEngine.insertScoreItem(id, (sid == null ? Std.random(4) : sid), self.game.flipCoordCase(x), y, time, null, true, false);
                nbPierres++;
              }
            }
          }
          self.game.perfectItemCpt = nbPierres;
        } else {
          old(self, id, sid);
        }
          
      }),

    ]);
  }

  public function execute(hf: Hf, specMan: SpecialManager, item: Item): Void {
    specMan.temporary(item.id, hf.Data.SECOND * 19);
    this.isActive = true;
    specMan.game.destroyList(hf.Data.BAD);
    specMan.game.destroyList(hf.Data.ITEM);
    specMan.game.destroyList(hf.Data.BAD_BOMB);
    specMan.levelConversion(hf.Data.CONVERT_DIAMANT, null);
  }

  public function interrupt(hf: Hf, specMan: SpecialManager): Void {
    specMan.clearRec();
    this.isActive = false;
    specMan.actives[this.id] = false;
  }

  public function skin(mc: MovieClip, subId: Null<Int>): Void {
    mc.gotoAndStop("" + (ID_PEARL + 1));
    var filter: ColorMatrixFilter = ColorMatrix.fromHsv(100, 1, 1).toFilter();
    mc.filters = [filter];
  }
}  